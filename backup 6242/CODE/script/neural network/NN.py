import tensorflow as tf
import json
import numpy as np
import pandas
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

time_interval = 10

class DataTranslate:
    def __init__(self, file_url):
        self.file_url = file_url

    def get_nn_input(self):
        df_total = pandas.read_csv(self.file_url)
        df = df_total.sample(frac = 0.5)
        df_remain = df_total.drop(df.index)
        feature = ['vendor_id', 'passenger_count', 'distance', 'pickup_day_of_week', 'pickup_longitude', \
                   'pickup_latitude', 'pickup_month', 'pickup_day', 'trip_duration']
        x = df[feature].values.tolist()
        duration = df['trip_duration'].values.tolist()
        label = df['label'].values.tolist()

        zero_vector = np.zeros([max(label) + 1]).tolist()

        y = []
        for i in range(len(label)):
            label_vector = list(zero_vector)
            label_vector[label[i]] = 1
            y.append(label_vector)

        x_remain = df_remain[feature].values.tolist()
        duration_remain = df_remain['trip_duration'].values.tolist()

        y_remain = []
        for i in range(len(label)):
            label_vector = list(zero_vector)
            label_vector[label[i]] = 1
            y_remain.append(label_vector)

        return x, y, duration, x_remain, y_remain, duration_remain

class NN:
    def __init__(self, config, input_, is_training):
        self.input = input_.x
        self.label = input_.y

        self._biuld_nn_graph(config, is_training)

        correct_prediction = tf.equal(tf.argmax(self.output, 1), tf.argmax(self.label, 1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        self.loss = -tf.reduce_mean(self.label * tf.log(self.output))

        if is_training:
            self.train = tf.train.AdamOptimizer(config.lr).minimize(self.loss)

    def _biuld_nn_graph(self, config, is_training):
        # add input layer
        # if is_training:
        #     input = tf.contrib.rnn.DropoutWrapper(self.input, output_keep_prob=config.keep_prob)
        W = tf.get_variable('input_w', [config.input_size, config.state_size], dtype = tf.float32)
        b = tf.get_variable('input_b', [config.state_size], dtype = tf.float32)
        state = tf.nn.tanh(tf.nn.xw_plus_b(self.input, W, b))

        # add hidden layer
        for i in range(config.hidden_layer_size):
            W = tf.get_variable('w_' + str(i), [config.state_size, config.state_size], dtype = tf.float32)
            b = tf.get_variable('b_' + str(i), [config.state_size], dtype=tf.float32)
            state = tf.nn.tanh(tf.nn.xw_plus_b(state, W, b))

        # add output layer
        W = tf.get_variable('output_w', [config.state_size, config.class_size], dtype = tf.float32)
        b = tf.get_variable('output_b', [config.class_size], dtype = tf.float32)
        self.output = tf.nn.softmax(tf.nn.xw_plus_b(state, W, b))

    def train_model(self, config, sess):
        for i in range(config.train_step_size):
            sess.run(self.train)
            if i % config.print_step_size == 0:
                loss = sess.run(self.loss)
                accuracy = sess.run(self.accuracy)
                print('the ' + str(i) + 'th step with loss ' + str(loss) + ' and accuracy ' + str(accuracy))

    def run_model(self, sess):
        output = sess.run(self.output)
        loss = sess.run(self.loss)
        accuracy = sess.run(self.accuracy)
        print('loss: ' + str(loss) + ' accuracy: ' + str(accuracy))
        return output, loss, accuracy

class Config:
    # graph configuration
    # the number of nodes in every hidden layer
    state_size = 200
    # the number of hidden layer
    hidden_layer_size = 2
    # keep probability
    keep_prob = 0.8

    # training configuration
    # total training steps
    train_step_size = 25
    # learning rate
    lr = 0.001
    # total number of the printing information
    print_step_size = 1

    def __init__(self, input_size, class_size):
        # input, output configuration
        # the feature size
        self.input_size = input_size
        # the number of classes (label number)
        self.class_size = class_size

class Input:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def create_nn_data():
    df = pandas.read_csv('remove_outliers.csv')
    predict_time = []

    tmp = 60 * time_interval
    l = range(60, 19000, tmp)
    for i in range(1, len(l)):
    	predict_time.append((l[i] + l[i-1]) / 2)

    def interval(s):
        # time gap in each label
        tmp = 60 * time_interval
        l = range(60, 19000, tmp)

        for i in range(len(l) - 1):
            if s <= l[i + 1]: return i
        return i + 1

    with open('label_time.json', 'w') as f:
    	json.dump(predict_time, f)
    df['label'] = df['trip_duration'].apply(interval)

    df.to_csv('NN_data_' + str(time_interval) + '.csv')

if __name__ == '__main__':

    create_nn_data()
    # exit(0)

    with tf.Session() as sess:
        print('reading data')
        dt = DataTranslate('NN_data_' + str(time_interval) + '.csv')
        x, y, duration, x_test, y_test, duration_test = dt.get_nn_input()

        print('converting data to tensor')
        tx = tf.constant(x, dtype = tf.float32)
        ty = tf.constant(y, dtype = tf.float32)

        input = Input(tx, ty)
        config = Config(len(x[0]), len(y[0]))

        print('building model')
        with tf.variable_scope('nn_classification', reuse = None):
            model = NN(config, input, is_training = True)
            sess.run(tf.global_variables_initializer())
            print('training')
            model.train_model(config, sess)

        print('training result')
        with tf.variable_scope('nn_classification', reuse = True):
            model = NN(config, input, is_training = False)
            output, loss, accuracy = model.run_model(sess)
            print('saving training result to file')
            with open('NN_train_result' + str(time_interval) + '.json', 'w') as f:
                json.dump({'output': output.tolist(), 'accuracy': accuracy.item(), 'loss': loss.item(), 'duration': duration}, f)

        print('converting test data to tensor')
        tx_test = tf.constant(x_test, dtype = tf.float32)
        ty_test = tf.constant(y_test, dtype = tf.float32)
        input = Input(tx_test, ty_test)

        print('test result')
        with tf.variable_scope('nn_classification', reuse = True):
            model = NN(config, input, is_training = False)
            output, loss, accuracy = model.run_model(sess)
            print('saving testing result to file')
            with open('NN_test_result' + str(time_interval) + '.json', 'w') as f:
                json.dump({'output': output.tolist(), 'accuracy': accuracy.item(), 'loss': loss.item(), 'duration': duration_test}, f)














