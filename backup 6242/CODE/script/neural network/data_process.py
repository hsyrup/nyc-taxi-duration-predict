import json
import numpy as np

def RSLM(prediction, y):
	return np.sqrt(sum(np.square(np.log(np.divide(np.array(prediction) + 1.0, np.array(y) + 1)))) / len(y))

def find_error_from_file(url):
	with open(url, 'r') as f:
		raw = json.load(f)
		prob = raw['output']
		y = raw['duration']

	pre = []
	for i in xrange(len(prob)):
		pre.append(label_time[np.array(prob[i]).argmax()])
	print 'error: ', RSLM(pre, y)

if __name__ == '__main__':
	# get label time
	with open('label_time.json', 'r') as f:
		label_time = json.load(f)

	print 'train data'
	find_error_from_file('NN_train_result10.json')

	print 'test data'
	find_error_from_file('NN_test_result10.json')






