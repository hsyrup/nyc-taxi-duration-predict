import xgboost as xgb
import pickle
import math
from xgboost import XGBClassifier
import numpy as np
from numpy import loadtxt
import pandas as pd
import plotly.graph_objs as go
import matplotlib as mp
import plotly.offline as py
from matplotlib import pylab as plt
import operator
py.init_notebook_mode(connected=True)
from ggplot import *

#---format the data----#
train = pd.read_csv('remove_outliers.csv')
scatter_data = train[train.trip_duration > 3600]
weather = pd.read_csv('weather_data.csv')
weather['date'] = pd.to_datetime(weather['date'])
weather['day_of_year'] = weather['date'].dt.dayofyear
weather['precipitation'] = np.where(weather['precipitation'] == 'T', '0.01', weather['precipitation'])
weather['snow fall'] = np.where(weather['snow fall'] == 'T', '0.01', weather['snow fall'])
weather['snow depth'] = np.where(weather['snow depth'] == 'T', '0.01', weather['snow depth'])
weather['snow depth'] = weather['snow depth'].apply(float)
weather['snow depth'] = np.where(weather['snow depth'] > 0, 0.2, weather['snow depth'])
weather['snow fall'] = weather['snow fall'].apply(float)
weather['precipitation'] = weather['precipitation'].apply(float)
weather['precipitation'] = np.where('precipitation' > 0, 0.2, weather['precipitation'])
weather_x = weather['day_of_year']
weather_pre = weather['precipitation']
weather_fall = weather['snow fall']
weather_depth = weather['snow depth']
train['dropoff_datetime'] = pd.to_datetime(train['dropoff_datetime'])
train['date']  = train['dropoff_datetime'].dt.date
train['date'] = pd.to_datetime(train.date)
weather['date'] = pd.to_datetime(weather.date)

#data used for train
newTrain = pd.merge(train, weather, how='left', on = 'date')
train = newTrain
lable = train['trip_duration']
feature = train.drop('trip_duration',1).drop('dropoff_datetime',1).drop('pickup_date',1).drop('date',1).drop('minimum temperature',1).drop('pickup_year',1).drop('snow fall',1).drop('vendor_id',1).drop('passenger_count',1).drop('average temperature',1).drop('maximum temperature',1)
feature = feature.drop('Unnamed: 0',1).drop('Unnamed: 0.1', 1)

#generate feature list used for training
featureList = list(feature.columns[0:])
featureList
def create_feature_map(featureList):
    outfile = open('outPut/xgb.fmap', 'w')
    i = 0
    for feat in featureList:
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
        i = i + 1
    outfile.close()
create_feature_map(featureList)

#set the parameters used for XGBoost
xgb_params = {"objective": "reg:linear", "booster":"gbtree", "eta": 0.3, "seed": 1300, "max_depth": 10, "silent": 1, "subsample": 0.9,
          "colsample_bytree": 0.7}
num_rounds = 300

#Split data into two parats to calculate the accuracy
featureTrain = feature[0:20000]
labelTrain = lable[0:20000]
featureTest = feature[20000:10000]
labelTest = lable[20000:10000]

#Use all of the data to train the model
dtrain = xgb.DMatrix(feature, label = lable)
gbdt = xgb.train(xgb_params, dtrain, num_rounds)
pickle.dump(gbdt, open("outPut/pima.pickle.dat", "wb"))
loaded_model = pickle.load(open("outPut/pima.pickle.dat", "rb"))

#Calcualte importance of each features
importance = loaded_model.get_score(importance_type='gain')
importance = sorted(importance.items(), key=operator.itemgetter(1))