Description:

There are two sub-directories in CODE directory, script and demo

CODE
	-script
		-data cleaning and model-based sampling
			-data cleaning and model-based sampling.ipynb
		-neural network
			-NN.py
			-data_process.py
	-xgboost
		-XGBoostTrain.py
	
	-demo
		-dva.py
		-geo-data.csv
		-index.html
		-public
		-trained-xgb.dat

data cleaning and model-based sampling.ipynb is an ipython notebook file used for data preprocessing, like filtering outliers and extracting features, explorative analysis and model-based sampling method.

NN.py is the modeling part for neural network which will generate output file in json format and data_process.py will use those output file as input and print the training and testing RMSLE. To use the model:
	1. Install the required libraries
	2. Include the input file named as 'remove_outliers.csv' under the same path as NN.py
	3. Run XGBoostTrain.py using python 2.7 and the training and testing result are ‘NN_train_result10.json’ and ‘NN_test_result10.json’
	4. Run data_process.py to see the RMSLE


XGBoostTrain.py is to predict the trip duration using gradient boosting method. To use the model:
	1. Install the required libraries
	2. Include the input file named as ’remove_outliers.csv' and 'weather_data.csv' under the same path as XGBoostTrain.py
	3. Run XGBoostTrain.py using python 3
	4. It will take over 20 minutes to train the model
	5. The trained model will be saved under outPut folder as 'pima.pickle.dat'
	6. Use pickle.load(open("outPut/pima.pickle.dat", "rb")) to import the trainded model into project
	7. Use the trainded model to predict duration time

demo directory holds codes for demo webpage. For more details of how to run demo, please refer execution part of this text file.



Installation:

You may need the following dependencies to run all scripts:

numpy
scipy
pandas
matplotlib
seaborn
sklearn
folium
cherrypy
xgboost
tensorflow
math
pickle
operator

If you only need to run demo, the following dependencies are needed:

numpy
pandas
cherrypy
xgboost

Once all dependencies are installed, you may go to next step and execute.



Execution:

Open terminal, change directory to ‘demo’ folder and run dva.py python script. After terminal shows some prompts, open your browser and go to http://127.0.0.1:8080 or whatever address the prompt says.

cd demp
python dva.py



Data:
You can download the dataset from https://www.kaggle.com/c/nyc-taxi-trip-duration/data, please download the train.zip.

