function scatterPlot(prediction){
    // Delete previous created SVG element
    d3.select("svg").remove();

    // Update top right corner to display predicted time
    $("#top-right-corner")
    .css("visibility", "visible")
    .text("The Predicted Travel Time is " + Math.ceil(prediction[1].predicted_time/60.0 ) + " min");

    var h = 770;
    var w = 1500;

    var domain = [0,100,400,700,1000,1300,1600,1900,2100];
    var colors = ["#f7fcfd","#e5f5f9","#ccece6","#99d8c9","#66c2a4","#41ae76","#238b45","#006d2c","#00441b"];
    // var domain = [100,400,700,1000,1300,1600,1900,2100];
    // var colors = ["#e5f5f9","#ccece6","#99d8c9","#66c2a4","#41ae76","#238b45","#006d2c","#00441b"];
    var colorscale = d3.scale.threshold()
                        .domain(domain)
                        .range(colors);

    //Create SVG element
    var svg = d3.select("#main-top-wrapper")
        .append("div")
        .attr("id", "svg_container")
        .append("svg")
        .attr("id", "svg")
        .attr("width", w)
        .attr("height", h);

    svg.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", w)
        .attr("height", h)
        .style("fill", "#DCDCDC");

    var beginning = [0,0];
    var end = [150,150,1000];
    var destination = [];
    prediction.forEach(function(d){
        if (d.id == 0){
            beginning[0] = (+d["longtitude"]+74.05)*1000;
            beginning[1] = (+d["latitude"]-40.6)*1000;
        }else if(d.id == 1){
            end[0] = (+d["longtitude"]+74.05)*1000;
            end[1] = (+d["latitude"]-40.6)*1000;
            end[2] = +d["predicted_time"];
        }else{
            destination.push([(+d["longtitude"]+74.05)*1000, (+d["latitude"]-40.6)*1000, +d["predicted_time"]]);
        }
    })

    var xscale = d3.scale.linear()
        .domain([0, 600])
        .range([0, 1200]);

    var yscale = d3.scale.linear()
        .domain([0, 300])
        .range([650,50]);

    var tip_dest = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d){
                    var tmp = "<div id='div'>";
                    tmp += "<div>" + ((d[0]/1000-74.05)*(-1)).toFixed(2) + " W,   " + (d[1]/1000+40.6).toFixed(2) + " N </div>";
                    tmp += "<div>Arriving " + (d[2]/60).toFixed(0) + " mins later</div>";
                    return tmp;
                });

    svg.call(tip_dest);

    svg.selectAll("circle")
        .data(destination)
        .enter()
        .append("circle")
        .attr("cx", function(d) {
            return xscale(d[0])+430;
        })
        .attr("cy", function(d) {
            return yscale(d[1]);
        })
        // .attr("r", function(d) {
        //     console.log(d);
        //     return rscale(d[2]);
        // })
        .attr("r", 2)
        .style("fill", function(d){
            return colorscale(d[2]);
            // if (d[0]>xmean){
            //     return d3.rgb("blue").toString();
            // }else{
            //     return d3.rgb("green").toString();
            // }
        })
        .on("mouseover", tip_dest.show)
        .on("mouseout", tip_dest.hide);


    //update begin
    var tip_begin = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html("<div id='div'><div>You are here ^_^</div>");

    svg.call(tip_begin);

    svg.append("circle")
            .attr("cx", xscale(beginning[0])+430)
            .attr("cy", yscale(beginning[1]))
            .attr("r", 5)
            .style("fill", "blueviolet");

    var img = svg.selectAll("image")
                .data([0])
                .enter()
                .append("svg:image")
                .attr("xlink:href", "/static/img/location.png")
                .attr("x", xscale(beginning[0])+430-15)
                .attr("y", yscale(beginning[1])-30)
                .attr("width", 30)
                .attr("height", 30)
                .on("click", tip_begin.show)
                .on("dblclick", tip_begin.hide);

    var tip_end = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html("<div id='div'><div>get it!</div>");

    svg.call(tip_end);

    svg.append("circle")
            .attr("cx", xscale(end[0])+430)
            .attr("cy", yscale(end[1]))
            .attr("r", 5)
            .style("fill","blue");
            

    svg.append("svg:image")
            .attr("xlink:href", "/static/img/location.png")
            .attr("x", xscale(end[0])+430-15)
            .attr("y", yscale(end[1])-30)
            .attr("width", 30)
            .attr("height", 30)
            .on("click", tip_end.show)
            .on("dblclick", tip_end.hide);

    var legend = svg.selectAll("rect")
                            .data(domain)
                            .enter()
                            
    legend.append("rect")
            .attr("x", 850)
            .attr("y", function(d, i){return i * 25+100;})
            .attr("width", 30)
            .attr("height", 23)
            .style("fill", function(d){return colorscale(d);});

    legend.append("text")
            .attr("x", 885)
            .attr("y", function(d, i){return i * 25 + 115;})
            .style("fill", "black")
            .text(function(d, i){
                return (d/60).toFixed(0) + " mins";
            });
    }