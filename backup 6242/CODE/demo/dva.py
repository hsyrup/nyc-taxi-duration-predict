import cherrypy
import os
import json
import numpy as np
import datetime as datetime
import pandas as pd
import pickle
import xgboost as xgb



lat_lon = pd.read_csv("geo-data.csv").iloc[:5000]

loaded_model = pickle.load(open("trained-xgb.dat","rb"))


class DVA(object):
    @cherrypy.expose
    def index(self):
        return open("index.html")

    @cherrypy.expose
    def predict(self,
                pickuplatitude,
                pickuplongitude,
                dropofflatitude,
                dropofflongitude,
                date,
                time,
                weather):
        # spatial informtaion
        pickup_lat = float(pickuplatitude)
        pickup_lon = float(pickuplongitude)
        dropoff_lat = float(dropofflatitude)
        dropoff_lon = float(dropofflongitude)
        def haversine_distance(pickup_lat, pickup_lon, dropoff_lat, dropoff_lon):
            lat1 = pickup_lat * np.pi / 180.0    
            lon1 = pickup_lon * np.pi / 180.0
            lat2 = dropoff_lat * np.pi / 180.0
            lon2 = dropoff_lon * np.pi / 180.0
            R = 6378137
            dlon = lon2 - lon1 
            dlat = lat2 - lat1 
            a1 = np.cos(lat1) * np.cos(lat2) * (np.sin(dlon/2))**2 
            c1 = 2 * np.arctan2(np.sqrt(a1), np.sqrt(1-a1)) 
            d1 = R * c1
            a2 = (np.sin(dlat/2))**2
            c2 = 2 * np.arctan2(np.sqrt(a2), np.sqrt(1-a2))
            d2 = R * c2
            haversine_distance = d1 + d2
            return haversine_distance
        distance = haversine_distance(pickup_lat, pickup_lon, dropoff_lat, dropoff_lon)

        # temporal information
        pickup_datetime = datetime.datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M")
        pickup_day = pickup_datetime.day
        pickup_day_of_week = pickup_datetime.weekday()
        pickup_month = pickup_datetime.month
        pickup_day_of_year = pickup_datetime.timetuple().tm_yday
        x = time.split(":")
        hour, minute = [int(x) for x in time.split(":")]
        pickup_time = 3600 * hour + minute * 60

        # weather information
        if weather == "other":
            snow_depth = 0.0
            precipitation = 0.0
        if weather == "rain":
            snow_depth = 0.0
            precipitation = 0.2
        if weather == "snow":
            snow_depth = 0.2
            precipitation = 0.0

        data = {"distance": distance,
                "pickup_day": pickup_day,
                "pickup_day_of_week": pickup_day_of_week,
                "pickup_time": pickup_time,
                "dropoff_longitude": dropoff_lon,
                "dropoff_latitude": dropoff_lat,
                "pickup_latitude": pickup_lat,
                "pickup_longitude": pickup_lon,
                "day_of_year": pickup_day_of_year,
                "pickup_month": pickup_month,
                "snow depth": snow_depth,
                "precipitation": precipitation}

        columns = ['pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 
            'dropoff_latitude', 'distance', 'pickup_time', 
            'pickup_day_of_week', 'pickup_month', 'pickup_day',
            'precipitation', 'snow depth', 'day_of_year']   
        df = pd.DataFrame([data])
        df = df.reindex_axis(columns, axis=1)

        n_spatial = 5000
        spatial = pd.concat([df]*(1+n_spatial), ignore_index=True)
        spatial.loc[1:, ['dropoff_latitude', 'dropoff_longitude']] = lat_lon

        spatial_preds = loaded_model.predict(xgb.DMatrix(spatial))
        spatial_prediction = [{"latitude": str(pickuplatitude),
                        "longtitude": str(pickuplongitude),
                        "predicted_time": str(0),
                        "id": 0}]

        for i, row in spatial.iterrows():
            spatial_prediction.append({"latitude": str(row['dropoff_latitude']),
                                        "longtitude": str(row['dropoff_longitude']),
                                        "predicted_time": str(spatial_preds[i]),
                                        "id": i+1})

        n_temporal = 144
        temporal = pd.concat([df]*(n_temporal), ignore_index=True)
        temporal['pickup_time'] = 600 * np.arange(144.0)
        # for i, row in temporal.iterrows():
        #     row['pickup_time'] = i * 600
        temporal_preds = loaded_model.predict(xgb.DMatrix(temporal))

        temporal_prediction = []
        for i, row in temporal.iterrows():
            temporal_prediction.append({"time": str(i*600),
                                        "predicted_time": str(temporal_preds[i])})

        return json.dumps(spatial_prediction) + "*" + json.dumps(temporal_prediction)


if __name__ == '__main__':
    conf = {
    '/': {
        'tools.sessions.on': True,
        'tools.staticdir.root': os.path.abspath(os.getcwd())
    },
    '/static': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './public'
    }
    }
    cherrypy.quickstart(DVA(), '/', conf)