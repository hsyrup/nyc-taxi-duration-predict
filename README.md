# VISUALIZATION
## New York City taxi duration prediction                                             

>Oct.-Dec. 2017    
>School of CSE, Georgia Tech                                          
>Supervisor: Prof. Duen Horng (Polo) Chau

* Visualized the prediction results in webpage using D3. Read prediction results from JSON file. Pick-up and drop-off location mark out with special 'png' images. Color of position changes gradually according to the prediction of duration. Tips of longitude, latitude and duration will show when mouse on positions. CSS decoration is applied.

## other team members' contribution

* Ruilin Li set the demo web environment
* Haoyan Zhai developed the NN model
* Xiaoyu Yang developed the XGboost model
* Yiou Zhu drew the poster